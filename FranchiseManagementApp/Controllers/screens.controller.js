﻿(function () {
    'use strict';

    angular.module('app.controllers').controller('ScreensController', ['$scope', 'LocationsService', 'ScreensService', 'PaginationService',
	function ($scope, LocationsService, ScreensService, PaginationService) {

	    var locationsService = LocationsService;
	    var screensService = ScreensService;
	    var pagination = PaginationService;

	    //$scope.screensList = { screens: [] };
	    $scope.screensList = screensService.getAllScreens();
	    //$scope.locationsList = { locations: [] };
	    $scope.locationsList = locationsService.getAllLocations();
	    //$scope.screensLoaded = { loaded: false };
	    $scope.screensLoaded = screensService.getScreensLoadStatus();

	    $scope.$watch('screensList.screens.length', function () {
	        if ($scope.screensList.screens != null) {
	            pagination.setDetails("Screens");
	        };
	    });

	    $scope.currentLocation = { name: "Default", l_id: 0, num_screens: 0 };
	    $scope.setCurrentLocation = function (num) {
	        if (num == 0) {
	            for (var i = 0; i <= $scope.locationsList.locations.length; i++) {
	                if ($scope.locationsList.locations[i].id == $scope.userProfile.profile.defaultLocation) {
	                    $scope.currentLocation.name = $scope.locationsList.locations[i].name;
	                    $scope.currentLocation.l_id = $scope.locationsList.locations[i].id;
	                    $scope.currentLocation.num_screens = $scope.locationsList.locations[i].num_screens;
	                    break;
	                };
	            };
	        }
	        else {
	            for (var i = 0; i <= $scope.locationsList.locations.length; i++) {
	                if ($scope.locationsList.locations[i].id == num) {
	                    $scope.currentLocation.name = $scope.locationsList.locations[i].name;
	                    $scope.currentLocation.l_id = $scope.locationsList.locations[i].id;
	                    $scope.currentLocation.num_screens = $scope.locationsList.locations[i].num_screens;
	                    break;
	                };
	            };
	        };  
	    };
        //Occurs on intial load of screen controller and loads user default location
	    if ($scope.currentLocation.name == "Default") {
	        $scope.setCurrentLocation(0);
	    };

	    $scope.screensDisplay = "ScreensList";
	    $scope.changeScreensDisplay = function (choice) {
	        $scope.screensDisplay = choice;
	    };

	    //Area for all Pagination details
	    $scope.paginationDetails = {};
	    $scope.paginationDetails = pagination.getDetails();
	    $scope.changePageNum = function (number) { pagination.changePageNum(number, "Screens"); };
	    $scope.firstPage = function () { pagination.firstPage(); };
	    $scope.lastPage = function () { pagination.lastPage(); };
	    $scope.prevPage = function () { pagination.prevPage(); };
	    $scope.nextPage = function () { pagination.nextPage(); };


	    $scope.blankScreen = {
	        id: -1,
	        number: 0,
	        capacity: 0,
            l_id: 0,
	        f_id: 1
	    };
	    $scope.newScreen = null;
	    $scope.newScreenSetup = function () {
	        $scope.newScreen = angular.copy($scope.blankScreen);
	        $scope.newScreen.l_id = $scope.currentLocation.l_id;
	    };
	    $scope.addScreen = function () {
	        screensService.QueryAddScreen($scope.newScreen);
	    }
	    $scope.tempScreen = null;
	    $scope.editScreenSetup = function (screen) {
	        $scope.tempScreen = angular.copy(screen);
	    };
	    $scope.editScreen = function () {
	        screensService.QueryEditScreen($scope.tempScreen);
	    }

	    $scope.deleteScreen = function (id) {
	        screensService.QueryDeleteScreen(id);
	    };

	    //Shows pop up modal to change location
	    $scope.changeLocationModal = function () {
	        $("#changeLocationModal").modal("toggle");
	    };

	    //Shows pop up modal to confirm deleting a location
	    $scope.deleteModal = function (screen) {
	        $("#deleteModal").modal("toggle");
	        $scope.tempScreen = angular.copy(screen);
	    };

	}]);

})();