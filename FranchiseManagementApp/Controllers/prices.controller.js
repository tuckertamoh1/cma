﻿(function () {
    'use strict';

    angular.module('app.controllers').controller('PricesController', ['$scope', 'LocationsService', 'PricesService', 'PaginationService',
	function ($scope, LocationsService, PricesService, PaginationService) {

	    var locationsService = LocationsService;
	    var pricesService = PricesService;
	    var pagination = PaginationService;

	    $scope.pricesList = pricesService.getAllPrices();
	    $scope.locationsList = locationsService.getAllLocations();
	    $scope.pricesLoaded = pricesService.getPricesLoadStatus();

	    $scope.blankPrice = {
	        id: -1,
	        f_id: 1,
	        l_id: 0,
	        priceType: "",
	        adult: 0,
	        student: 0,
	        child: 0,
	        oap: 0
	    };

	    $scope.aEmpty = true;
	    $scope.eEmpty = true;

	    $scope.$watch('pricesList.prices.length', function () {
	        if ($scope.pricesList.prices != null) {
	            pagination.setDetails("Prices");
	        };
	    });

	    $scope.setPricesForDisplay = function () {
	        //Following is used to display afternoon + evening prices
	        for (var i = 0; i < $scope.pricesList.prices.length; i++) {
	            if ($scope.pricesList.prices[i].l_id == $scope.currentLocation.l_id && $scope.pricesList.prices[i].priceType == "afternoon") {
	                $scope.afternoon = angular.copy($scope.pricesList.prices[i]);
	                $scope.aEmpty = false;
	            }
	            else if ($scope.pricesList.prices[i].l_id == $scope.currentLocation.l_id && $scope.pricesList.prices[i].priceType == "evening") {
	                $scope.evening = angular.copy($scope.pricesList.prices[i]);
	                $scope.eEmpty = false;
	            };
	        };

	        if ($scope.aEmpty == true) {
	            $scope.afternoon = angular.copy($scope.blankPrice);
	            $scope.afternoon.priceType = "afternoon";
	        };
	        if ($scope.eEmpty == true) {
	            $scope.evening = angular.copy($scope.blankPrice);
	            $scope.evening.priceType = "evening";
	        };
	    };

	    $scope.currentLocation = { name: "Default", l_id: 0 };
	    $scope.setCurrentLocation = function (num) {
	        if (num == 0) {
	            for (var i = 0; i <= $scope.locationsList.locations.length; i++) {
	                if ($scope.locationsList.locations[i].id == $scope.userProfile.profile.defaultLocation) {
	                    $scope.currentLocation.name = $scope.locationsList.locations[i].name;
	                    $scope.currentLocation.l_id = $scope.locationsList.locations[i].id;
	                    break;
	                };
	            };
	        }
	        else {
	            for (var i = 0; i <= $scope.locationsList.locations.length; i++) {
	                if ($scope.locationsList.locations[i].id == num) {
	                    $scope.currentLocation.name = $scope.locationsList.locations[i].name;
	                    $scope.currentLocation.l_id = $scope.locationsList.locations[i].id;
	                    break;
	                };
	            };
	        };
	        $scope.setPricesForDisplay();
	        $scope.changePrices1 = false;
	        $scope.changePrices2 = false;
	    };
        
	    //Reg Ex fo whole number     "/^[0-9]{1,7}$/"

	    //Occurs on intial load of prices controller and loads user default location
	    $scope.changePrices1 = false;
	    $scope.afternoon = angular.copy($scope.blankPrice);
	    $scope.changePrices2 = false;
	    $scope.evening = angular.copy($scope.blankPrice);
	    if ($scope.currentLocation.name == "Default") {
	        $scope.setCurrentLocation(0);
	        $scope.setPricesForDisplay();
	    };

	    $scope.enablePriceChange1 = function (num) {
	        if (num == 1) {     //Edit
	            $scope.changePrices1 = true;
	        }
	        else if (num == 2) {    //Save
	            $scope.changePrices1 = false;
	            if ($scope.afternoon.id == -1) {
	                $scope.afternoon.l_id = $scope.currentLocation.l_id;
	                pricesService.QueryAddPrice($scope.afternoon);
	            }
	            else {
	                $scope.afternoon.l_id = $scope.currentLocation.l_id;
	                pricesService.QueryEditPrice($scope.afternoon);
	            };
	        }
	        else {  //Cancel
	            $scope.changePrices1 = false;
	            $scope.setPricesForDisplay();
	        }
	    };

	    $scope.enablePriceChange2 = function (num) {
	        if (num == 1) {     //Edit
	            $scope.changePrices2 = true;
	        }
	        else if (num == 2) {    //Save
	            $scope.changePrices2 = false;
	            if ($scope.evening.id == -1) {
	                $scope.evening.l_id = $scope.currentLocation.l_id;
	                pricesService.QueryAddPrice($scope.evening);
	            }
	            else {
	                $scope.evening.l_id = $scope.currentLocation.l_id;
	                pricesService.QueryEditPrice($scope.evening);
	            };
	        }
	        else {      //Cancel
	            $scope.changePrices2 = false;
	            $scope.setPricesForDisplay();
	        }
	    };

	    $scope.pricesDisplay = "PricesList";
	    $scope.changePricesDisplay = function (choice) {
	        $scope.pricesDisplay = choice;
	    };

	    //Shows pop up modal to change location
	    $scope.changeLocationModal = function (price) {
	        $("#changeLocationModal").modal("toggle");
	    };

	    
	}]);
})();