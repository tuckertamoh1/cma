﻿(function () {
    'use strict';

    angular.module('app.controllers').controller('AccountsController', ['$scope', 'AccountsService', 'LocationsService', 'PaginationService',
	function ($scope, AccountsService, LocationsService, PaginationService) {

	    var accountsService = AccountsService;
	    var locationsService = LocationsService;
	    var pagination = PaginationService;

	    //$scope.accountsList = { accounts: [] };
	    $scope.accountsList = accountsService.getAllAccounts();
	    $scope.locationsList = locationsService.getAllLocations();

	    //$scope.accountsLoaded = { loaded: false };
	    $scope.accountsLoaded = accountsService.getAccountsLoadStatus();

	    $scope.$watch('accountsList.accounts.length', function () {
	        if ($scope.accountsList.accounts != null) {
	            pagination.setDetails("Accounts");
	        };
	    });

	    $scope.accountsDisplay = "AccountsList";
	    $scope.changeAccountsDisplay = function (choice) {
	        $scope.accountsDisplay = choice;
	    };

	    //Area for all Pagination details
	    $scope.paginationDetails = {};
	    $scope.paginationDetails = pagination.getDetails();
	    $scope.changePageNum = function (number) {pagination.changePageNum(number, "Accounts");};
	    $scope.firstPage = function () {pagination.firstPage();};
	    $scope.lastPage = function () {pagination.lastPage();};
	    $scope.prevPage = function () {pagination.prevPage();};
	    $scope.nextPage = function () { pagination.nextPage(); };


	    $scope.blankAccount = {
	        id: -1,
	        f_name: "",
	        l_name: "",
	        prof_img: "",
	        username: "",
	        password: "",
	        email: "",
	        mobile: 0,
	        userType: "Location Manager",
	        defaultLocation: 0,
	        f_id: 1
	    };
	    $scope.newAccount = null;
	    $scope.newAccountSetup = function () {
	        $scope.newAccount = angular.copy($scope.blankAccount);
	        $scope.defaultLocation1.name = "None Selected";
	        $scope.previewProfileImg1 = false;
	    };
	    $scope.addAccount = function () {
	        accountsService.QueryAddAccount($scope.newAccount);
	    }
	    $scope.tempAccount = null;
	    $scope.editAccountSetup = function (account) {
	        $scope.tempAccount = angular.copy(account);
	        var temp = locationsService.getLocation(account.defaultLocation);
	        $scope.changeDefaultLocation1(temp);
	        $scope.previewProfileImg1 = false;
	    };
	    $scope.editAccount = function () {
	        accountsService.QueryEditAccount($scope.tempAccount, 2);
	    }

	    $scope.deleteAccount = function (id) {
	        accountsService.QueryDeleteAccount(id);
	    };

	    //Shows pop up modal to confirm deleing a user account
	    $scope.deleteModal = function (account) {
	        $("#deleteModal").modal("toggle");
	        $scope.tempAccount = angular.copy(account);
	    };

	    //List of user types
	    $scope.userType = ["Franchise Manager", "Location Manager"];

	    $scope.defaultLocation1 = { name: "None Selected" };
	    $scope.changeDefaultLocation1 = function (location, num) {
	        $scope.defaultLocation1.name = location.name;
	        if (num == 1) {
	            $scope.newAccount.defaultLocation = location.id;
	        }
	        else {
	            $scope.tempAccount.defaultLocation = location.id;
	        };   
	    };

	    $scope.previewProfileImg1 = false;
	    $scope.showProfileImg1 = true;
	    $scope.profileImg1 = null;
	    $scope.onLoad1 = function (e, reader, file, fileList, fileOjects, fileObj) {
	        $scope.previewProfileImg1 = true;
	    };

	    $scope.deleteProfileImage1 = function () {
	        $scope.tempAccount.prof_img = "";
	    };
	    $scope.changeProfileImage1 = function (b64, num) {
	        if(num == 1) {
	            $scope.newAccount.prof_img = angular.copy(b64);
	        }
	        else {
	            $scope.tempAccount.prof_img = angular.copy(b64);
	        };
	        $scope.previewProfileImg1 = false;
	        $scope.profileImg1 = null;
	    };
	    $scope.discardUploadedImage1 = function () {
	        $scope.previewProfileImg1 = false;
	        $scope.profileImg1 = null;
	    };

	}]);

})();