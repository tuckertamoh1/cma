﻿(function () {
    'use strict';

    angular.module('app.controllers').controller('IndexController', ['$scope', '$q', '$location', 'LoginService', 'ProfileService','LocationsService', 'AccountsService',
	function ($scope, $q, $location, LoginService, ProfileService, LocationsService, AccountsService) {

	    var loginService = LoginService;
	    var profileService = ProfileService;
	    var locationsService = LocationsService;
	    var accountsService = AccountsService;

	    //$scope.userProfile = { profile: [] };
	    $scope.userProfile = profileService.getUserProfile();
	    $scope.locationsList = locationsService.getAllLocations();
	    
	    if ($.cookie("FMS_LoggedIn") == "true") {
	        //userService.QueryAllUsers();
	        //loginService.userLogin("tamoh1", "123");
	    }
	    else {
	        $location.path('/');
	    };

	    $scope.logout = function () {
	        loginService.logout();
	    };

	    $scope.displayOpt = "Default";
	    $scope.changeDisplayOpt = function ( choice ) {
	        $scope.displayOpt = choice;
	    };

	    $scope.showSideBar = true;
	    $scope.toggleSideBar = function () {
	        if ($scope.showSideBar == true) {
	            $scope.showSideBar = false;
	            $("#contentArea").toggleClass("toggled");
	        }
	        else {
	            $scope.showSideBar = true;
	            $("#contentArea").toggleClass("toggled");
	        }   
	    };

        //List of user types
	    $scope.userType = ["manager", "supervisor"];

	    $scope.tempProfile = "";
	    $scope.editAccount = function () {
	        profileService.QueryEditProfile($scope.tempProfile);
	    }

	    //Tabs to navigate between details for adding, editing a store in modals
	    $scope.activeTab = 1;
	    $scope.setActiveTab = function (tabToSet) { $scope.activeTab = tabToSet; };
	    $scope.editTab = "tab1";
	    $scope.setEditTab = function (name) { $scope.editTab = name; };

	    //Edit profile pop up modal
	    $scope.profileModal = function () {
	        $("#profileModal").modal("toggle");
	        //$scope.showProfileImg = true;
	        $scope.tempProfile = angular.copy($scope.userProfile.profile);
	        var temp = locationsService.getLocation($scope.userProfile.profile.defaultLocation);
	        $scope.changeDefaultLocation(temp);
	    };

	    $scope.defaultLocation = { name: "temp" };
	    $scope.changeDefaultLocation = function (location) {
	        $scope.defaultLocation.name = location.name;
	        $scope.tempProfile.defaultLocation = location.id;
	    };


	    $scope.previewProfileImg = false;
	    $scope.showProfileImg = true;
	    $scope.profileImg = null;
	    $scope.onLoad = function (e, reader, file, fileList, fileOjects, fileObj) {
	        $scope.previewProfileImg = true;
	    };

	    //$scope.resizeImage(file, fileObj);

	    //$scope.resizeImage = function (file, base64_object) {
	    //    // file is an instance of File constructor.
	    //    // base64_object is an object that contains compiled base64 image data from file.
	    //    var deferred = $q.defer();
	    //    var url = URL.createObjectURL(file);// creates url for file object.
	    //    var test = "";
	    //    Jimp.read(url)
        //    .then(function (item) {
        //        item
        //        .resize(100, Jimp.AUTO)// width of 1280px, auto-adjusted height
        //        //.quality(50)//drops the image quality to 50%
        //        .getBase64(file.type, function (err, newBase64) {
        //            if (err) { throw err; }
        //            var bytes = Math.round((3 / 4) * newBase64.length);
        //            base64_object.filetype = file.type;
        //            base64_object.filesize = bytes;
        //            test = newBase64.substring(22);
        //            base64_object.base64 = test;
        //            // Note that base64 in this package doesn't contain "data:image/jpeg;base64," part,
        //            // while base64 string from Jimp does. It should be taken care of in back-end side.
        //            deferred.resolve(base64_object);
        //        });
        //    })
        //    .catch(function (err) {
        //        return console.log(err);// error handling
        //    });
	    //    return deferred.promise;
	    //};

	    $scope.deleteProfileImage = function () {
	        $scope.tempProfile.prof_img = "";
	    };
	    $scope.changeProfileImage = function ( b64) {
	        $scope.tempProfile.prof_img = angular.copy(b64);
	        $scope.previewProfileImg = false;
	        $scope.profileImg = null;
	    };
	    $scope.discardUploadedImage = function () {
	        $scope.previewProfileImg = false;
	        $scope.profileImg = null;
	    };

	}]);

})();