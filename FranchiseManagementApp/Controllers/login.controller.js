﻿(function () {
    'use strict';

    angular.module('app.controllers').controller('LoginController', ['$scope', '$location', 'LoginService',
	function ($scope, $location, LoginService) {

	    var loginService = LoginService;

	    $scope.username = "";
	    $scope.password = "";
	    $scope.cookieLogin = { status: false };
	    $scope.cookieLogin = loginService.getCookieStatus();
	    $scope.loginFail = loginService.getLoginFail();
	    

	    $scope.userLogin = function () {
	        loginService.userLogin($scope.username, $scope.password);
	        //loginService.addUser();
	    };

	    //Method is used to redirect user if they are already
	    //logged in to the system
	    $scope.checkLogin = function () {
	        if ($.cookie("FMS_LoggedIn") == "true") {
	            console.log("Login Status: true");
	            $location.path('/index');
	        }
	        else {
	            //Do nothing and stay on login page
	            console.log("Login Status: false");
	        };
	    };

	    $scope.checkLogin();


	}]);

})();