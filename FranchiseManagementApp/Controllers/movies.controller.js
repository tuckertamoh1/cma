﻿(function () {
    'use strict';

    angular.module('app.controllers').controller('MoviesController', ['$scope', '$q', 'LocationsService', 'MoviesService', 'MovieScheduleService', 'PaginationService',
	function ($scope, $q, LocationsService, MoviesService, MovieScheduleService, PaginationService) {

	    var locationsService = LocationsService;
	    var moviesService = MoviesService;
	    var movSchedService = MovieScheduleService;
	    var pagination = PaginationService;

	    $scope.moviesList = moviesService.getAllMovies();
	    $scope.locationsList = locationsService.getAllLocations();
	    $scope.moviesLoaded = moviesService.getMoviesLoadStatus();

	    $scope.$watch('moviesList.movies.length', function () {
	        if ($scope.moviesList.movies != null) {
	            pagination.setDetails("Movies");
	        };
	    });

	    $scope.currentLocation = { name: "Default", l_id: 0 };
	    $scope.setCurrentLocation = function (num) {
	        if (num == 0) {
	            for (var i = 0; i < $scope.locationsList.locations.length; i++) {
	                if ($scope.locationsList.locations[i].id == $scope.userProfile.profile.defaultLocation) {
	                    $scope.currentLocation.name = $scope.locationsList.locations[i].name;
	                    $scope.currentLocation.l_id = $scope.locationsList.locations[i].id;
	                    break;
	                };
	            };
	        }
	        else {
	            for (var i = 0; i < $scope.locationsList.locations.length; i++) {
	                if ($scope.locationsList.locations[i].id == num) {
	                    $scope.currentLocation.name = $scope.locationsList.locations[i].name;
	                    $scope.currentLocation.l_id = $scope.locationsList.locations[i].id;
	                    break;
	                };
	            };
	        };
	    };
	    //Occurs on intial load of screen controller and loads user default location
	    if ($scope.currentLocation.name == "Default") {
	        $scope.setCurrentLocation(0);
	    };

	    $scope.moviesDisplay = "MoviesList";
	    $scope.changeMoviesDisplay = function (choice) {
	        $scope.moviesDisplay = choice;
	    };

	    //Area for all Pagination details
	    $scope.paginationDetails = {};
	    $scope.paginationDetails = pagination.getDetails();
	    $scope.changePageNum = function (number) { pagination.changePageNum(number, "Movies"); };
	    $scope.firstPage = function () { pagination.firstPage(); };
	    $scope.lastPage = function () { pagination.lastPage(); };
	    $scope.prevPage = function () { pagination.prevPage(); };
	    $scope.nextPage = function () { pagination.nextPage(); };


	    $scope.blankMovie = {
	        id: -1,
	        title: "",
            description:"",
            movie_img: "",
            age_rating: "G – General Audiences",
            duration: 0,
            genre: "",
	        l_id: 0,
	        f_id: 1
	    };
	    $scope.newMovie = null;
	    $scope.newMovieSetup = function () {
	        $scope.newMovie = angular.copy($scope.blankMovie);
	        $scope.newMovie.l_id = $scope.currentLocation.l_id;
	        $scope.previewProfileImg1 = false;
	    };
	    $scope.addMovie = function () {
	        moviesService.QueryAddMovie($scope.newMovie);
	    }
	    $scope.tempMovie = null;
	    $scope.editMovieSetup = function (movie) {
	        $scope.tempMovie = angular.copy(movie);
	        $scope.previewProfileImg1 = false;
	    };
	    $scope.editMovie = function () {
	        moviesService.QueryEditMovie($scope.tempMovie);
	    }

	    $scope.deleteMovie = function (movie) {
	        moviesService.QueryDeleteMovie(movie.id);
	        movSchedService.QueryDeleteSchedulesForMovie(movie.l_id, movie.id);
	    };

	    //Shows pop up modal to change location
	    $scope.changeLocationModal = function () {
	        $("#changeLocationModal").modal("toggle");
	    };

	    //Shows pop up modal to confirm deleting a location
	    $scope.deleteModal = function (movie) {
	        $("#deleteModal").modal("toggle");
	        $scope.tempMovie = angular.copy(movie);
	    };

	    //List of user types
	    $scope.ageTypes = [
                            "G – General Audiences",
                            "PG – Parental Guidance Suggested",
                            "PG-13 – Parents Strongly Cautioned",
                            "R – Restricted",
                            "NC-17 – Adults Only"];

	    $scope.previewProfileImg1 = false;
	    $scope.showProfileImg1 = true;
	    $scope.profileImg1 = null;
	    $scope.resizeImage = function (file, base64_object) {
	        // file is an instance of File constructor.
	        // base64_object is an object that contains compiled base64 image data from file.
	        var deferred = $q.defer();
	        var url = URL.createObjectURL(file);// creates url for file object.
	        var test = "";
	        Jimp.read(url)
            .then(function (item) {
                item
                .resize(100, Jimp.AUTO)// width of 1280px, auto-adjusted height
                //.quality(50)//drops the image quality to 50%
                .getBase64(file.type, function (err, newBase64) {
                    if (err) { throw err; }
                    var bytes = Math.round((3 / 4) * newBase64.length);
                    base64_object.filetype = file.type;
                    base64_object.filesize = bytes;
                    test = newBase64.substring(22);
                    //console.log(test);
                    base64_object.base64 = test;
                    $scope.profileImg1 = angular.copy(base64_object.base64);
                    // Note that base64 in this package doesn't contain "data:image/jpeg;base64," part,
                    // while base64 string from Jimp does. It should be taken care of in back-end side.
                    // console.log(base64_object.base64);
                    deferred.resolve(base64_object);
                });
            })
            .catch(function (err) {
                return console.log(err);// error handling
            });
	        return deferred.promise;
	    };
	    $scope.onLoad1 = function (e, reader, file, fileList, fileOjects, fileObj) {
	        $scope.previewProfileImg1 = true;
	        if (fileObj.filesize > 40000) {
                $scope.resizeImage(file, fileObj);  //auto resizes all profile images to 100x100px
	        }
	        
	    };
	    $scope.deleteProfileImage1 = function () {
	        $scope.tempMovie.movie_img = "";
	    };
	    $scope.changeProfileImage1 = function (b64, num) {
	        if (num == 1) {
	            $scope.newMovie.movie_img = angular.copy(b64);
	        }
	        else {
	            $scope.tempMovie.movie_img = angular.copy(b64);
	        };
	        $scope.previewProfileImg1 = false;
	        $scope.profileImg1 = null;
	    };
	    $scope.discardUploadedImage1 = function () {
	        $scope.previewProfileImg1 = false;
	        $scope.profileImg1 = null;
	    };

	}]);
})();