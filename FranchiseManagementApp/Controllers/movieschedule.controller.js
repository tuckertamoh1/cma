﻿(function () {
    'use strict';

    angular.module('app.controllers').controller('MovieScheduleController', ['$scope', 'MovieScheduleService', 'LocationsService', 'ScreensService', 'MoviesService', 'PaginationService',
	function ($scope, MovieScheduleService, LocationsService, ScreensService, MoviesService, PaginationService) {

	    var movSchedService = MovieScheduleService;
	    var locationsService = LocationsService;
	    var screensService = ScreensService;
	    var moviesService = MoviesService;
	    var pagination = PaginationService;

	    //$scope.movSchedList = { schedules: [] };
	    $scope.movSchedList = movSchedService.getAllMovieSchedules();
	    //$scope.screensList = { screens: [] };
	    $scope.screensList = screensService.getAllScreens();
	    //$scope.moviesList = { movies: [] };
	    $scope.moviesList = moviesService.getAllMovies();
	    //$scope.locationsList = { locations: [] };
	    $scope.locationsList = locationsService.getAllLocations();
	    $scope.movSchedLoaded = { loaded: false };
	    $scope.movSchedLoaded = movSchedService.getMovSchedLoadStatus();

        //Lists for Screens + Movies for a location
	    $scope.movSchedScreens = { screens: [] };
	    $scope.movSchedMovies = { movies: [] };

        //Variables to track current screen and movie for that screen
	    $scope.movSchedCurrScreen = { screen: 0 };
	    $scope.movSchedCurrMovie = { movie: "None Selected" };

	    $scope.$watch('movSchedList.schedules.length', function () {
	        if ($scope.movSchedList.schedules != null) {
	            pagination.setDetails("MovieSchedule");
	        };
	    });

	    $scope.currentLocation = { name: "Default", l_id: 0, num_screens: 0 };
	    $scope.setCurrentLocation = function (num) {
	        //sets current location
	        if (num == 0) {
	            for (var i = 0; i < $scope.locationsList.locations.length; i++) {
	                if ($scope.locationsList.locations[i].id == $scope.userProfile.profile.defaultLocation) {
	                    $scope.currentLocation.name = $scope.locationsList.locations[i].name;
	                    $scope.currentLocation.l_id = $scope.locationsList.locations[i].id;
	                    $scope.currentLocation.num_screens = $scope.locationsList.locations[i].num_screens;
	                    break;
	                };
	            };
	        }
	        else {
	            for (var i = 0; i < $scope.locationsList.locations.length; i++) {
	                if ($scope.locationsList.locations[i].id == num) {
	                    $scope.currentLocation.name = $scope.locationsList.locations[i].name;
	                    $scope.currentLocation.l_id = $scope.locationsList.locations[i].id;
	                    $scope.currentLocation.num_screens = $scope.locationsList.locations[i].num_screens;
	                    break;
	                };
	            };
	        };

	        //sets screens and movies for location
	        for (var i = 0; i < $scope.screensList.screens.length; i++) {
	            if ($scope.screensList.screens[i].l_id == $scope.currentLocation.l_id) {
	                $scope.movSchedScreens.screens.push(angular.copy($scope.screensList.screens[i]));
	            };
	        };
	        for (var i = 0; i < $scope.moviesList.movies.length; i++) {
	            if ($scope.moviesList.movies[i].l_id == $scope.currentLocation.l_id) {
	                $scope.movSchedMovies.movies.push(angular.copy($scope.moviesList.movies[i]));
	            };
	        };
	    };
	    //Occurs on intial load of screen controller and loads user default location
	    if ($scope.currentLocation.name == "Default") {
	        $scope.setCurrentLocation(0);
	    };

	    $scope.movieSchedDisplay = "MovieScheduleList";
	    $scope.changeMovieSchedDisplay = function (choice) {
	        $scope.movieSchedDisplay = choice;
	    };

	    //Area for all Pagination details
	    $scope.paginationDetails = {};
	    $scope.paginationDetails = pagination.getDetails();
	    $scope.changePageNum = function (number) { pagination.changePageNum(number, "MovieSchedule"); };
	    $scope.firstPage = function () { pagination.firstPage(); };
	    $scope.lastPage = function () { pagination.lastPage(); };
	    $scope.prevPage = function () { pagination.prevPage(); };
	    $scope.nextPage = function () { pagination.nextPage(); };

	    //Shows pop up modal to change location
	    $scope.changeLocationModal = function () {
	        $("#changeLocationModal").modal("toggle");
	    };
	    //Shows pop up modal to change movie
	    $scope.changeMovieModal = function () {
	        $("#changeMovieModal").modal("toggle");
	    };

	    //Setion to edit screen schedule
	    $scope.blankMovSched = {
	        id: -1,
	        f_id: 1,
            l_id: 0,
	        s_id: 0,
            m_id: 0,
            movie_time: "",
            curr_status: "active",
            movie_name: "None Selected",
            tickets_booked: 0,
            screen_num: 0
	    };
	    $scope.tempSched = {};
	    $scope.numShowings = { number: 0 };
	    $scope.newTime = { time: 0 };
	    $scope.setupEditSchedule = function (screen) {
	        $scope.tempSched = angular.copy($scope.blankMovSched);
	        $scope.tempSched.l_id = angular.copy($scope.currentLocation.l_id);
	        $scope.tempSched.s_id = angular.copy(screen.id);
	        $scope.tempSched.screen_num = angular.copy(screen.number);

	        $scope.movSchedCurrScreen.screen = angular.copy(screen.id);

	        //Following is used to set the name of the moive for a screen that is selected
	        if ( $scope.movSchedList.schedules.length != 0 ) { 
	            for (var i = 0; i < $scope.movSchedList.schedules.length; i++) {
	                if ($scope.movSchedList.schedules[i].s_id == $scope.movSchedCurrScreen.screen) {
	                    $scope.movSchedCurrMovie.movie = $scope.movSchedList.schedules[i].m_id;
	                    for (var j = 0; j < $scope.movSchedMovies.movies.length; j++) {
	                        if ($scope.movSchedMovies.movies[j].id == $scope.movSchedCurrMovie.movie) {
	                            $scope.tempSched.movie_name = angular.copy($scope.movSchedMovies.movies[j].title);
	                            $scope.tempSched.m_id = $scope.movSchedCurrMovie.movie;
	                            break;
	                        };
	                    };
	                    break;
	                };
	            };
	        };
	    };

	    $scope.tempDataToDeleteScheds = {};
	    $scope.setCurrentMovie = function (movie) {
	        $scope.tempDataToDeleteScheds = angular.copy($scope.tempSched);
	        $scope.tempSched.movie_name = angular.copy(movie.title);
	        $scope.tempSched.m_id = angular.copy(movie.id);
	        $scope.movSchedCurrMovie.movie = angular.copy(movie.id);
	    };
	    $scope.saveChangeMovie = function () {
            movSchedService.QueryDeleteAllMovieSchedule($scope.tempSched.l_id, $scope.tempSched.s_id);
            //Delete all current schedules
	    };
	    $scope.cancelChangeMovie = function () {
	        for (var j = 0; j < $scope.movSchedScreens.screens.length; j++) {
	            if ($scope.movSchedScreens.screens[j].id == $scope.movSchedCurrScreen.screen) {
	                $scope.setupEditSchedule($scope.movSchedScreens.screens[j]);
	                break;
	            };
	        };
	    };

	    $scope.saveSchedule = function () {
	        movSchedService.QueryAddMovieSchedule($scope.tempSched);
	    };











	    //$scope.blankScreen = {
	    //    id: -1,
	    //    number: 0,
	    //    capacity: 0,
	    //    l_id: 0,
	    //    f_id: 1
	    //};
	    //$scope.newScreen = null;
	    //$scope.newScreenSetup = function () {
	    //    $scope.newScreen = angular.copy($scope.blankScreen);
	    //    $scope.newScreen.l_id = $scope.currentLocation.l_id;
	    //};
	    //$scope.addScreen = function () {
	    //    screensService.QueryAddScreen($scope.newScreen);
	    //}
	    //$scope.tempScreen = null;
	    //$scope.editScreenSetup = function (screen) {
	    //    $scope.tempScreen = angular.copy(screen);
	    //};
	    //$scope.editScreen = function () {
	    //    screensService.QueryEditScreen($scope.tempScreen);
	    //}

	    //$scope.deleteScreen = function (id) {
	    //    screensService.QueryDeleteScreen(id);
	    //};

	    

	    ////Shows pop up modal to confirm deleting a location
	    //$scope.deleteModal = function (screen) {
	    //    $("#deleteModal").modal("toggle");
	    //    $scope.tempScreen = angular.copy(screen);
	    //};

	}]);

})();