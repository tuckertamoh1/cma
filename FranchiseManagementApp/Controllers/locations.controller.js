﻿(function () {
    'use strict';

    angular.module('app.controllers').controller('LocationsController', ['$scope', 'LocationsService', 'PaginationService',
	function ($scope, LocationsService, PaginationService) {

	    var locationsService = LocationsService;
	    var pagination = PaginationService;

	    //$scope.locationsList = { locations: [] };
	    $scope.locationsList = locationsService.getAllLocations();

	    //$scope.locationsLoaded = { loaded: false };
	    $scope.locationsLoaded = locationsService.getLocationsLoadStatus();

	    $scope.$watch('locationsList.locations.length', function () {
	        if ($scope.locationsList.locations != null) {
	            pagination.setDetails("Locations");
	        };
	    });

	    $scope.locationsDisplay = "LocationsList";
	    $scope.changeLocationsDisplay = function (choice) {
	        $scope.locationsDisplay = choice;
	    };

	    //Area for all Pagination details
	    $scope.paginationDetails = {};
	    $scope.paginationDetails = pagination.getDetails();
	    $scope.changePageNum = function (number) { pagination.changePageNum(number, "Locations"); };
	    $scope.firstPage = function () { pagination.firstPage(); };
	    $scope.lastPage = function () { pagination.lastPage(); };
	    $scope.prevPage = function () { pagination.prevPage(); };
	    $scope.nextPage = function () { pagination.nextPage(); };

	    //List of location status options
	    $scope.locationStatusOpt = ["active", "inactive"];

	    $scope.blankLocation = {
	        id: -1,
	        name: "",
	        address: "",
	        num_screens: 0,
            l_status: "inactive",
	        f_id: 1
	    };
	    $scope.newLocation = null;
	    $scope.newLocationSetup = function () {
	        $scope.newLocation = angular.copy($scope.blankLocation);
	    };
	    $scope.addLocation = function () {
	        locationsService.QueryAddLocation($scope.newLocation);
	    }
	    $scope.tempLocation = null;
	    $scope.editLocationSetup = function (location) {
	        $scope.tempLocation = angular.copy(location);
	    };
	    $scope.editLocation = function () {
	        locationsService.QueryEditLocation($scope.tempLocation);
	    }

	    $scope.deleteLocation = function (id) {
	        locationsService.QueryDeleteLocation(id);
	    };

	    //Shows pop up modal to confirm deleting a location
	    $scope.deleteModal = function (location) {
	        $("#deleteModal").modal("toggle");
	        $scope.tempLocation = angular.copy(location);
	    };

	}]);

})();