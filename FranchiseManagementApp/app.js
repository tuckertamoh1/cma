﻿(function () {
    'use strict';

    angular.module('managementApp', ['ngRoute', 'app.controllers', 'app.services', 'app.directives', 'app.routes', 'flow', 'base64', 'naif.base64', 'moment-picker']);

})();

//.run(['$', '$rootScope', '$location', function ($, $rootScope, $location) { }])