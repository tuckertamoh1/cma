﻿(function () {
    'use strict';

    angular.module('app.services').factory('LocationsService', ['$rootScope', '$http', 'UrlService',
        function ($rootScope, $http, UrlService) {

            var self = this;

            var urlService = UrlService;

            var locationsList = { locations: [] };
            var locationsLoaded = { loaded: false };

            var serverUrl = { url: "" };
            serverUrl = urlService.getUrl();

            this.getLocationsLoadStatus = function () {
                return locationsLoaded;
            };

            this.getLocation = function (id) {
                for (var i = 0; i < locationsList.locations.length; i++) {
                    if (locationsList.locations[i].id == id) {
                        return angular.copy(locationsList.locations[i]);
                        break;
                    };
                };
            };

            this.getAllLocations = function () {
                return locationsList;
            };

            this.QueryAllLocations = function (f_id) {
                var temp = serverUrl.url + "getAllLocations";
                $http({
                    method: 'POST',
                    url: temp,
                    data: {id: f_id }
                }).then(function successCallback(response) {
                    //console.log(response.data);
                    if (response.data != "None") {
                        locationsList.locations = response.data;
                    };
                    locationsLoaded.loaded = true;
                }, function errorCallback(response) {
                    console.log("Error!!!!!");
                });
            };

            this.QueryAddLocation = function (newLocation) {
                var temp = serverUrl.url + "addNewLocation";
                locationsList.locations.push(angular.copy(newLocation));
                $http({
                    method: 'POST',
                    url: temp,
                    data: newLocation
                }).then(function successCallback(response) {
                    console.log(response.data);
                    //Find the account with the id and delete it and update server
                    for (var i = 0; i < locationsList.locations.length; i++) {
                        if (locationsList.locations[i].id == -1) {
                            locationsList.locations[i].id = parseInt(response.data);
                        };
                    };
                }, function errorCallback(response) {
                    console.log("Error with sending add to server!!");
                });
            };

            this.QueryEditLocation = function (editLocation) {
                var temp = serverUrl.url + "updateLocation";
                //Update the current active profile
                for (var i = 0; i < locationsList.locations.length; i++) {
                    if (locationsList.locations[i].id == editLocation.id) {
                        locationsList.locations[i] = angular.copy(editLocation);
                        $http({
                            method: 'POST',
                            url: temp,
                            data: editLocation
                        }).then(function successCallback(response) {
                            console.log(response.data);

                        }, function errorCallback(response) {
                            console.log("Error with sending udate to server!!");
                        });
                        break;
                    };
                };
            };

            this.QueryDeleteLocation = function (l_id) {
                var temp = serverUrl.url + "deleteLocation";
                //Find the location with the id and delete it and update server
                for (var i = 0; i < locationsList.locations.length; i++) {
                    if (locationsList.locations[i].id == l_id) {
                        locationsList.locations.splice(i, 1);
                        $http({
                            method: 'POST',
                            url: temp,
                            data: { id: l_id }
                        }).then(function successCallback(response) {
                            console.log(response.data);
                        }, function errorCallback(response) {
                            console.log("Delete Error!!!!");
                        });
                        break;
                    };
                };
            };
            return self;
        }]);

})();