﻿(function () {
    'use strict';

    angular.module('app.services').factory('LoginService', ['$rootScope', '$location', 'UrlService', 'ProfileService', 'AccountsService', 'LocationsService', 'ScreensService', 'MoviesService', 'PricesService', 'MovieScheduleService', '$http',
        function ($rootScope, $location, UrlService, ProfileService, AccountsService, LocationsService, ScreensService, MoviesService, PricesService, MovieScheduleService, $http) {

            var self = this;

            var urlService = UrlService;
            var profileService = ProfileService;
            var accountsService = AccountsService;
            var locationsService = LocationsService;
            var screensService = ScreensService;
            var moviesService = MoviesService;
            var pricesService = PricesService;
            var movSchedService = MovieScheduleService;

            var cookieLogin = { status: $.cookie("FMS_LoggedIn") };
            var loginStatus = { status: false };
            var loginFail = { failed: false };

            var serverUrl = { url: "" };
            serverUrl = urlService.getUrl();

            this.getLoginStatus = function () {
                return loginStatus;
            };
            this.setLoginStatus = function (status) {
                loginStatus.status = status;
            };
            this.getCookieStatus = function () {
                return cookieLogin;
            };
            this.getLoginFail = function () {
                return loginFail;
            };
            this.setLoginFail = function () {
                loginFail.failed = false;
            };

            this.userLogin = function (u_name, p_word) {
                $http({
                    method: 'POST',
                    url: serverUrl.url+"login",
                    data: { username: u_name, password: p_word },
                    //headers: { 'Content-Type': 'application/json' }
                }).then(function successCallback(response) {
                    console.log(response.data);
                    if (response.data != "Failed") {
                        
                        //Set the profile of the current active user
                        profileService.setUserProfile(response.data[0]);

                        //Choose what is to be loaded on successful login
                        accountsService.QueryAllAccounts(response.data[0].id, response.data[0].f_id);
                        locationsService.QueryAllLocations(response.data[0].f_id);
                        screensService.QueryAllScreens(response.data[0].f_id);
                        moviesService.QueryAllMovies(response.data[0].f_id);
                        pricesService.QueryAllPrices(response.data[0].f_id);
                        movSchedService.QueryAllMovieSchedules(response.data[0].f_id);



                        loginFail.failed = false;
                        loginStatus.status = true;
                        $.cookie("FMS_LoggedIn", true);
                        cookieLogin.status = true;
                        $location.path('/index');
                    }
                    else {
                        loginFail.failed = true;
                        loginStatus.status = false;
                        $.cookie("FMS_LoggedIn", false);
                        cookieLogin.status = false;
                    };

                    //$rootScope.$apply();

                    //userList.uList = response.data;
                    // this callback will be called asynchronously
                    // when the response is available
                }, function errorCallback(response) {
                    console.log("Login Error!!!!!");
                });
            };

            this.logout = function () {
                loginStatus.status = false;
                $.cookie("FMS_LoggedIn", false);
                cookieLogin.status = false;
                $location.path('/');
            };

            return self;

        }]);

})();