﻿(function () {
    'use strict';

    angular.module('app.services').factory('AccountsService', ['$rootScope', '$http', 'UrlService',
        function ($rootScope, $http, UrlService) {

            var self = this;

            var urlService = UrlService;

            var accountsList = { accounts: [] };
            var accountsLoaded = { loaded: false };

            var serverUrl = { url: "" };
            serverUrl = urlService.getUrl();

            this.getAccountsLoadStatus = function () {
                return accountsLoaded;
            };

            this.getAllAccounts = function () {
                return accountsList;
            };

            this.QueryAllAccounts = function (u_id, fran_id) {
                var temp = serverUrl.url + "getAllUserAccounts";
                $http({
                    method: 'POST',
                    url: temp,
                    data: { id: u_id, f_id: fran_id }
                }).then(function successCallback(response) {
                    //console.log(response.data);
                    if( response.data != "None") {
                        accountsList.accounts = response.data;
                    };
                    accountsLoaded.loaded = true;
                }, function errorCallback(response) {
                    console.log("Error!!!!!");
                });
            };

            this.QueryAddAccount = function (account) {
                var temp = serverUrl.url + "addNewUserAccount";
                accountsList.accounts.push(angular.copy(account));
                $http({
                    method: 'POST',
                    url: temp,
                    data: account
                }).then(function successCallback(response) {
                    console.log(response.data);
                    //Find the account with the id and delete it and update server
                    for (var i = 0; i < accountsList.accounts.length; i++) {
                        if (accountsList.accounts[i].id == -1) {
                            accountsList.accounts[i].id = parseInt(response.data);
                        };
                    };
                }, function errorCallback(response) {
                    console.log("Error with sending add to server!!");
                });
            };

            this.QueryEditAccount = function (account) {
                var temp = serverUrl.url + "updateUserAccount";
                //Update the current active profile
                for (var i = 0; i < accountsList.accounts.length; i++) {
                    if (accountsList.accounts[i].id == account.id) {
                        accountsList.accounts[i] = angular.copy(account);
                        $http({
                            method: 'POST',
                            url: temp,
                            data: account
                        }).then(function successCallback(response) {
                            console.log(response.data);

                        }, function errorCallback(response) {
                            console.log("Error with sending udate to server!!");
                        });
                        break;
                    };
                };
            };

            this.QueryDeleteAccount = function (u_id) {
                var temp = serverUrl.url + "deleteUserAccount";
                //Find the account with the id and delete it and update server
                for (var i = 0; i < accountsList.accounts.length; i++) {
                    if (accountsList.accounts[i].id == u_id) {
                        accountsList.accounts.splice(i, 1);
                        $http({
                            method: 'POST',
                            url: temp,
                            data: { id: u_id }
                        }).then(function successCallback(response) {
                            console.log(response.data);
                        }, function errorCallback(response) {
                            console.log("Delete Error!!!!");
                        });
                        break;
                    };
                };
            };
            return self;
    }]);

})();