﻿(function () {
    'use strict';

    angular.module('app.services').factory('MovieScheduleService', ['$rootScope', '$http', 'UrlService',
        function ($rootScope, $http, UrlService) {

            var self = this;

            var urlService = UrlService;

            var movSchedList = { schedules: [] };
            var movSchedLoaded = { loaded: false };

            var serverUrl = { url: "" };
            serverUrl = urlService.getUrl();

            this.getMovSchedLoadStatus = function () {
                return movSchedLoaded;
            };

            this.getAllMovieSchedules = function () {
                return movSchedList;
            };

            this.QueryAllMovieSchedules = function (f_id) {
                var temp = serverUrl.url + "getAllMovieSchedules";
                $http({
                    method: 'POST',
                    url: temp,
                    data: { id: f_id }
                }).then(function successCallback(response) {
                    //console.log(response.data);
                    if (response.data != "None") {
                        movSchedList.schedules = response.data;
                    };
                    movSchedLoaded.loaded = true;
                }, function errorCallback(response) {
                    console.log("Error!!!!!");
                });
            };

            this.QueryAddMovieSchedule = function (movSched) {
                var temp = serverUrl.url + "addNewMovieSchedule";
                movSchedList.schedules.push(angular.copy(movSched));
                $http({
                    method: 'POST',
                    url: temp,
                    data: movSched
                }).then(function successCallback(response) {
                    console.log(response.data);
                    //Find the account with the id and delete it and update server
                    for (var i = 0; i < movSchedList.schedules.length; i++) {
                        if (movSchedList.schedules[i].id == -1) {
                            movSchedList.schedules[i].id = parseInt(response.data);
                        };
                    };
                }, function errorCallback(response) {
                    console.log("Error with sending add to server!!");
                });
            };

            this.QueryEditMovieSchedule = function (movSched) {
                var temp = serverUrl.url + "updateMovieSchedule";
                //Update the current active profile
                for (var i = 0; i < movSchedList.schedules.length; i++) {
                    if (movSchedList.schedules[i].id == movSched.id) {
                        movSchedList.schedules[i] = angular.copy(movSched);
                        $http({
                            method: 'POST',
                            url: temp,
                            data: movSched
                        }).then(function successCallback(response) {
                            console.log(response.data);

                        }, function errorCallback(response) {
                            console.log("Error with sending udate to server!!");
                        });
                        break;
                    };
                };
            };

            this.QueryDeleteAllMovieSchedule = function (loc_id, scr_id) {
                var temp = serverUrl.url + "deleteAllMovieSchedule";
                //Delete all schedules for a screen
                $http({
                    method: 'POST',
                    url: temp,
                    data: { l_id: loc_id, s_id: scr_id }
                }).then(function successCallback(response) {
                    console.log(response.data);
                }, function errorCallback(response) {
                    console.log("Delete Error!!!!");
                });

                for (var i = 0; i < movSchedList.schedules.length; i++) {
                    if (movSchedList.schedules[i].s_id == scr_id) {
                        movSchedList.schedules.splice(i, 1);
                    };
                };
            };

            this.QueryDeleteSchedulesForMovie = function (loc_id, mov_id) {
                var temp = serverUrl.url + "deleteSchedulesForMovie";
                //Delete all schedules for a screen
                $http({
                    method: 'POST',
                    url: temp,
                    data: { l_id: loc_id, m_id: mov_id }
                }).then(function successCallback(response) {
                    console.log(response.data);
                }, function errorCallback(response) {
                    console.log("Delete Error!!!!");
                });

                for (var i = movSchedList.schedules.length -1; i >= 0 ; i--) {
                    if (movSchedList.schedules[i].l_id == loc_id && movSchedList.schedules[i].m_id == mov_id) {
                        movSchedList.schedules.splice(i, 1);
                    };
                };
            };

            this.QueryDeleteMovieSchedule = function (ms_id) {
                var temp = serverUrl.url + "deleteMovieSchedule";
                //Find the location with the id and delete it and update server
                for (var i = 0; i < movSchedList.schedules.length; i++) {
                    if (movSchedList.schedules[i].id == s_id) {
                        movSchedList.schedules.splice(i, 1);
                        $http({
                            method: 'POST',
                            url: temp,
                            data: { id: ms_id }
                        }).then(function successCallback(response) {
                            console.log(response.data);
                        }, function errorCallback(response) {
                            console.log("Delete Error!!!!");
                        });
                        break;
                    };
                };
            };
            return self;
        }]);

})();