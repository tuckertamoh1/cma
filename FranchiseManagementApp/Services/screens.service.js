﻿(function () {
    'use strict';

    angular.module('app.services').factory('ScreensService', ['$rootScope', '$http', 'UrlService',
        function ($rootScope, $http, UrlService) {

            var self = this;

            var urlService = UrlService;

            var screensList = { screens: [] };
            var screensLoaded = { loaded: false };

            var serverUrl = { url: "" };
            serverUrl = urlService.getUrl();

            this.getScreensLoadStatus = function () {
                return screensLoaded;
            };

            this.getAllScreens = function () {
                return screensList;
            };

            this.QueryAllScreens = function (f_id) {
                var temp = serverUrl.url + "getAllScreens";
                $http({
                    method: 'POST',
                    url: temp,
                    data: { id: f_id }
                }).then(function successCallback(response) {
                    //console.log(response.data);
                    if (response.data != "None") {
                        screensList.screens = response.data;
                    };
                    screensLoaded.loaded = true;
                }, function errorCallback(response) {
                    console.log("Error!!!!!");
                });
            };

            this.QueryAddScreen = function (screen) {
                var temp = serverUrl.url + "addNewScreen";
                screensList.screens.push(angular.copy(screen));
                $http({
                    method: 'POST',
                    url: temp,
                    data: screen
                }).then(function successCallback(response) {
                    console.log(response.data);
                    //Find the account with the id and delete it and update server
                    for (var i = 0; i < screensList.screens.length; i++) {
                        if (screensList.screens[i].id == -1) {
                            screensList.screens[i].id = parseInt(response.data);
                        };
                    };
                }, function errorCallback(response) {
                    console.log("Error with sending add to server!!");
                });
            };

            this.QueryEditScreen = function (screen) {
                var temp = serverUrl.url + "updateScreen";
                //Update the current active profile
                for (var i = 0; i < screensList.screens.length; i++) {
                    if (screensList.screens[i].id == screen.id) {
                        screensList.screens[i] = angular.copy(screen);
                        $http({
                            method: 'POST',
                            url: temp,
                            data: screen
                        }).then(function successCallback(response) {
                            console.log(response.data);

                        }, function errorCallback(response) {
                            console.log("Error with sending udate to server!!");
                        });
                        break;
                    };
                };
            };

            this.QueryDeleteScreen = function (s_id) {
                var temp = serverUrl.url + "deleteScreen";
                //Find the location with the id and delete it and update server
                for (var i = 0; i < screensList.screens.length; i++) {
                    if (screensList.screens[i].id == s_id) {
                        screensList.screens.splice(i, 1);
                        $http({
                            method: 'POST',
                            url: temp,
                            data: { id: s_id }
                        }).then(function successCallback(response) {
                            console.log(response.data);
                        }, function errorCallback(response) {
                            console.log("Delete Error!!!!");
                        });
                        break;
                    };
                };
            };
            return self;
        }]);

})();