﻿(function () {
    'use strict';

    angular.module('app.services').factory('UrlService', [
        function () {

            var self = this;
            var serverUrl = { url: "http://192.168.1.11:3458/" };       //H
            //var serverUrl = { url: "http://192.168.43.235:3458/" };   //M

            this.getUrl = function () {
                return serverUrl
            };
            

            return self;

        }]);

})();