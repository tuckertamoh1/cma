﻿(function () {
    'use strict';

    angular.module('app.services').factory('ProfileService', ['$rootScope', '$http', 'UrlService',
        function ($rootScope, $http, UrlService) {

            var self = this;

            var urlService = UrlService;

            var userProfile = { profile: [] };

            var serverUrl = { url: "" };
            serverUrl = urlService.getUrl();

            this.getUserProfile = function () {
                return userProfile;
            };

            this.setUserProfile = function (u_profile) {
                userProfile.profile = u_profile;
            };

            this.QueryEditProfile = function (u_profile) {
                var temp = serverUrl.url + "updateUserAccount";
                //Update the current profile
                $http({
                    method: 'POST',
                    url: temp,
                    data: u_profile
                }).then(function successCallback(response) {
                    console.log(response.data);
                    userProfile.profile = u_profile;
                }, function errorCallback(response) {
                    console.log("Error with sending profile udate to server!!");
                });
            };

            return self;
        }]);
})();