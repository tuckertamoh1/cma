﻿(function () {
    'use strict';

    angular.module('app.services').factory('MoviesService', ['$rootScope', '$http', 'UrlService',
        function ($rootScope, $http, UrlService) {

            var self = this;

            var urlService = UrlService;

            var moviesList = { movies: [] };
            var moviesLoaded = { loaded: false };

            var serverUrl = { url: "" };
            serverUrl = urlService.getUrl();

            this.getMoviesLoadStatus = function () {
                return moviesLoaded;
            };

            this.getMovie = function (id) {
                for (var i = 0; i < moviesList.movies.length; i++) {
                    if (moviesList.movies[i].id == id) {
                        return angular.copy(moviesList.movies[i]);
                        break;
                    };
                };
            };

            this.getAllMovies = function () {
                return moviesList;
            };

            this.QueryAllMovies = function (f_id) {
                var temp = serverUrl.url + "getAllMovies";
                $http({
                    method: 'POST',
                    url: temp,
                    data: { id: f_id }
                }).then(function successCallback(response) {
                    //console.log(response.data);
                    if (response.data != "None") {
                        moviesList.movies = response.data;
                    };
                    moviesLoaded.loaded = true;
                }, function errorCallback(response) {
                    console.log("Error!!!!!");
                });
            };

            this.QueryAddMovie = function (movie) {
                var temp = serverUrl.url + "addNewMovie";
                moviesList.movies.push(angular.copy(movie));
                $http({
                    method: 'POST',
                    url: temp,
                    data: movie
                }).then(function successCallback(response) {
                    console.log(response.data);
                    //Find the account with the id and delete it and update server
                    for (var i = 0; i < moviesList.movies.length; i++) {
                        if (moviesList.movies[i].id == -1) {
                            moviesList.movies[i].id = parseInt(response.data);
                        };
                    };
                }, function errorCallback(response) {
                    console.log("Error with sending add to server!!");
                });
            };

            this.QueryEditMovie = function (movie) {
                var temp = serverUrl.url + "updateMovie";
                //Update the current active profile
                for (var i = 0; i < moviesList.movies.length; i++) {
                    if (moviesList.movies[i].id == location.id) {
                        moviesList.movies[i] = angular.copy(movie);
                        $http({
                            method: 'POST',
                            url: temp,
                            data: movie
                        }).then(function successCallback(response) {
                            console.log(response.data);

                        }, function errorCallback(response) {
                            console.log("Error with sending udate to server!!");
                        });
                        break;
                    };
                };
            };

            this.QueryDeleteMovie = function (m_id) {
                var temp = serverUrl.url + "deleteMovie";
                //Find the movie with the id and delete it and update server
                for (var i = 0; i < moviesList.movies.length; i++) {
                    if (moviesList.movies[i].id == m_id) {
                        moviesList.movies.splice(i, 1);
                        $http({
                            method: 'POST',
                            url: temp,
                            data: { id: m_id}
                        }).then(function successCallback(response) {
                            console.log(response.data);
                        }, function errorCallback(response) {
                            console.log("Delete Error!!!!");
                        });
                        break;
                    };
                };
            };
            return self;
        }]);

})();