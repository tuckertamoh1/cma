﻿(function () {
    'use strict';

    angular.module('app.services').factory('PricesService', ['$rootScope', '$http', 'UrlService',
        function ($rootScope, $http, UrlService) {

            var self = this;

            var urlService = UrlService;

            var pricesList = { prices: [] };
            var pricesLoaded = { loaded: false };

            var serverUrl = { url: "" };
            serverUrl = urlService.getUrl();

            this.getPricesLoadStatus = function () {
                return pricesLoaded;
            };

            this.getPrice = function (id) {
                for (var i = 0; i < pricesList.prices.length; i++) {
                    if (pricesList.prices[i].id == id) {
                        return angular.copy(pricesList.prices[i]);
                        break;
                    };
                };
            };

            this.getAllPrices = function () {
                return pricesList;
            };

            this.QueryAllPrices = function (f_id) {
                var temp = serverUrl.url + "getAllPrices";
                $http({
                    method: 'POST',
                    url: temp,
                    data: { id: f_id }
                }).then(function successCallback(response) {
                    //console.log(response.data);
                    if (response.data != "None") {
                        pricesList.prices = response.data;
                    };
                    pricesLoaded.loaded = true;
                }, function errorCallback(response) {
                    console.log("Error!!!!!");
                });
            };

            this.QueryAddPrice = function (price) {
                var temp = serverUrl.url + "addNewPrice";
                pricesList.prices.push(angular.copy(price));
                $http({
                    method: 'POST',
                    url: temp,
                    data: price
                }).then(function successCallback(response) {
                    console.log(response.data);
                    //Find the account with the id and delete it and update server
                    for (var i = 0; i < pricesList.prices.length; i++) {
                        if (pricesList.prices[i].id == -1) {
                            pricesList.prices[i].id = parseInt(response.data);
                        };
                    };
                }, function errorCallback(response) {
                    console.log("Error with sending add to server!!");
                });
            };

            this.QueryEditPrice = function (price) {
                var temp = serverUrl.url + "updatePrice";
                //Update the current active profile
                for (var i = 0; i < pricesList.prices.length; i++) {
                    if (pricesList.prices[i].id == price.id) {
                        pricesList.prices[i] = angular.copy(price);
                        $http({
                            method: 'POST',
                            url: temp,
                            data: price
                        }).then(function successCallback(response) {
                            console.log(response.data);

                        }, function errorCallback(response) {
                            console.log("Error with sending udate to server!!");
                        });
                        break;
                    };
                };
            };

            this.QueryDeletePrice = function (p_id) {
                var temp = serverUrl.url + "deletePrice";
                //Find the price with the id and delete it and update server
                for (var i = 0; i < pricesList.prices.length; i++) {
                    if (pricesList.prices[i].id == l_id) {
                        pricesList.prices.splice(i, 1);
                        $http({
                            method: 'POST',
                            url: temp,
                            data: { id: p_id }
                        }).then(function successCallback(response) {
                            console.log(response.data);
                        }, function errorCallback(response) {
                            console.log("Delete Error!!!!");
                        });
                        break;
                    };
                };
            };
            return self;
        }]);

})();