﻿(function () {
    'use strict';

    angular.module('app.services').factory('PaginationService', ['AccountsService', 'LocationsService', 'ScreensService', 'MoviesService', 'PricesService', 'MovieScheduleService',
        function (AccountsService, LocationsService, ScreensService, MoviesService, PricesService, MovieScheduleService) {
            var self = this;
            var accountsService = AccountsService;
            var locationsService = LocationsService;
            var screensService = ScreensService;
            var moviesService = MoviesService;
            var pricesService = PricesService;
            var movSchedService = MovieScheduleService;

            var accountsList = accountsService.getAllAccounts();
            var locationsList = locationsService.getAllLocations();
            var screensList = screensService.getAllScreens();
            var moviesList = moviesService.getAllMovies();
            var pricesList = pricesService.getAllPrices();
            var movSchedList = movSchedService.getAllMovieSchedules();

            var paginationDetails = {
                pageSize: [10, 20, 30, 40, 50],
                totalPages: 0,
                pageNum: 1,
                displayNum: 10,
                startNum: 0,
                endNum: 10
            };

            this.getDetails = function () {
                return paginationDetails;
            };

            this.setDetails = function (listType) {
                paginationDetails.displayNum = 10;
                paginationDetails.pageNum = 1;
                paginationDetails.startNum = 0;
                paginationDetails.endNum = paginationDetails.displayNum;
                if (listType == "Accounts") {
                    paginationDetails.totalPages = Math.ceil(accountsList.accounts.length / paginationDetails.displayNum);
                }
                else if (listType == "Locations") {
                    paginationDetails.totalPages = Math.ceil(locationsList.locations.length / paginationDetails.displayNum);
                }
                else if (listType == "Screens") {
                    paginationDetails.totalPages = Math.ceil(screensList.screens.length / paginationDetails.displayNum);
                }
                else if (listType == "Movies") {
                    paginationDetails.totalPages = Math.ceil(moviesList.movies.length / paginationDetails.displayNum);
                }
                else if (listType == "Prices") {
                    paginationDetails.totalPages = Math.ceil(pricesList.prices.length / paginationDetails.displayNum);
                }
                else if (listType == "MovieSchedule") {
                    paginationDetails.totalPages = Math.ceil(movSchedList.schedules.length / paginationDetails.displayNum);
                };
            };

            this.changePageNum = function (num, listType) {
                paginationDetails.displayNum = num;
                paginationDetails.pageNum = 1;
                paginationDetails.startNum = 0;
                paginationDetails.endNum = paginationDetails.displayNum;
                if (listType == "Accounts") {
                    paginationDetails.totalPages = Math.ceil(accountsList.accounts.length / paginationDetails.displayNum);
                }
                else if (listType == "Locations") {
                    paginationDetails.totalPages = Math.ceil(locationsList.locations.length / paginationDetails.displayNum);
                }
                else if (listType == "Screens") {
                    paginationDetails.totalPages = Math.ceil(screensList.screens.length / paginationDetails.displayNum);
                }
                else if (listType == "Movies") {
                    paginationDetails.totalPages = Math.ceil(moviesList.movies.length / paginationDetails.displayNum);
                }
                else if (listType == "Prices") {
                    paginationDetails.totalPages = Math.ceil(pricesList.prices.length / paginationDetails.displayNum);
                }
                else if (listType == "MovieSchedule") {
                    paginationDetails.totalPages = Math.ceil(movSchedList.schedules.length / paginationDetails.displayNum);
                };
            };

            this.firstPage = function () {
                paginationDetails.pageNum = 1;
                paginationDetails.startNum = (paginationDetails.pageNum * paginationDetails.displayNum) - paginationDetails.displayNum;
                paginationDetails.endNum = (paginationDetails.pageNum * paginationDetails.displayNum);
            };

            this.lastPage = function () {
                paginationDetails.pageNum = paginationDetails.totalPages;
                paginationDetails.startNum = (paginationDetails.pageNum * paginationDetails.displayNum) - paginationDetails.displayNum;
                paginationDetails.endNum = (paginationDetails.pageNum * paginationDetails.displayNum);
            };

            this.nextPage = function () {
                if (paginationDetails.pageNum < paginationDetails.totalPages) {
                    paginationDetails.pageNum++;
                    paginationDetails.startNum = (paginationDetails.pageNum * paginationDetails.displayNum) - paginationDetails.displayNum;
                    paginationDetails.endNum = (paginationDetails.pageNum * paginationDetails.displayNum);
                }
            };

            this.prevPage = function () {
                if (paginationDetails.pageNum > 1) {
                    paginationDetails.pageNum--;
                    paginationDetails.startNum = (paginationDetails.pageNum * paginationDetails.displayNum) - paginationDetails.displayNum;
                    paginationDetails.endNum = (paginationDetails.pageNum * paginationDetails.displayNum);
                }
            };
            return self;
        }]);

})();





