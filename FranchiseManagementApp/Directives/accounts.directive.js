﻿(function () {
    'use strict';

    angular.module('app.directives').directive('accountsDirective', function () {

        return {
            templateUrl: "Templates/Accounts/accounts.html",
            controller: "AccountsController"
        }
    });

    angular.module('app.directives').directive('accountsListDirective', function () {

        return {
            templateUrl: "Templates/Accounts/accountslist.html"
        }
    });

    angular.module('app.directives').directive('accountsAddDirective', function () {

        return {
            templateUrl: "Templates/Accounts/addaccount.html"
        }
    });

    angular.module('app.directives').directive('accountsEditDirective', function () {

        return {
            templateUrl: "Templates/Accounts/editaccount.html"
        }
    });

})();