﻿(function () {
    'use strict';

    angular.module('app.directives').directive('pricesDirective', function () {

        return {
            templateUrl: "Templates/Prices/prices.html",
            controller: "PricesController"
        }
    });

    angular.module('app.directives').directive('pricesListDirective', function () {

        return {
            templateUrl: "Templates/Prices/priceslist.html"
        }
    });

})();