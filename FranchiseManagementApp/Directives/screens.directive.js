﻿(function () {
    'use strict';

    angular.module('app.directives').directive('screensDirective', function () {

        return {
            templateUrl: "Templates/Screens/screens.html",
            controller: "ScreensController"
        }
    });

    angular.module('app.directives').directive('screensListDirective', function () {

        return {
            templateUrl: "Templates/Screens/screenslist.html"
        }
    });

    angular.module('app.directives').directive('screensAddDirective', function () {

        return {
            templateUrl: "Templates/Screens/addscreen.html"
        }
    });

    angular.module('app.directives').directive('screensEditDirective', function () {

        return {
            templateUrl: "Templates/Screens/editscreen.html"
        }
    });

})();