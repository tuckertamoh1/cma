﻿(function () {
    'use strict';

    angular.module('app.directives').directive('moviesDirective', function () {

        return {
            templateUrl: "Templates/Movies/movies.html",
            controller: "MoviesController"
        }
    });

    angular.module('app.directives').directive('moviesListDirective', function () {

        return {
            templateUrl: "Templates/Movies/movieslist.html"
        }
    });

    angular.module('app.directives').directive('moviesAddDirective', function () {

        return {
            templateUrl: "Templates/Movies/addmovie.html"
        }
    });

    angular.module('app.directives').directive('moviesEditDirective', function () {

        return {
            templateUrl: "Templates/Movies/editmovie.html"
        }
    });

})();