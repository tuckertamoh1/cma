﻿(function () {
    'use strict';

    angular.module('app.directives').directive('locationsDirective', function () {

        return {
            templateUrl: "Templates/Locations/locations.html",
            controller: "LocationsController"
        }
    });

    angular.module('app.directives').directive('locationsListDirective', function () {

        return {
            templateUrl: "Templates/Locations/locationslist.html"
        }
    });

    angular.module('app.directives').directive('locationsAddDirective', function () {

        return {
            templateUrl: "Templates/Locations/addlocation.html"
        }
    });

    angular.module('app.directives').directive('locationsEditDirective', function () {

        return {
            templateUrl: "Templates/Locations/editlocation.html"
        }
    });

})();