﻿(function () {
    'use strict';

    angular.module('app.directives').directive('topNavDirective', function () {

        return {
            templateUrl: "Templates/Index/TopNavBar.html"
            //,
            //controller: "DashboardController"
        }
    });

    angular.module('app.directives').directive('toggleNavDirective', function () {

        return {
            templateUrl: "Templates/Index/ToggleNavBar.html"
            //,
            //controller: "DashboardController"
        }
    });

    angular.module('app.directives').directive('contentAreaDirective', function () {

        return {
            templateUrl: "Templates/Index/ContentArea.html"
            //,
            //controller: "DashboardController"
        }
    });

    angular.module('app.directives').directive('sideBarDirective', function () {

        return {
            templateUrl: "Templates/Index/SideBar.html"
            //,
            //controller: "DashboardController"
        }
    });

})();