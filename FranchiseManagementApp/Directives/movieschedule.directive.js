﻿(function () {
    'use strict';

    angular.module('app.directives').directive('moviescheduleDirective', function () {

        return {
            templateUrl: "Templates/MovieSchedule/movieschedule.html",
            controller: "MovieScheduleController"
        }
    });

    angular.module('app.directives').directive('moviescheduleListDirective', function () {

        return {
            templateUrl: "Templates/MovieSchedule/movieschedulelist.html"
        }
    });

    angular.module('app.directives').directive('screenschedEditDirective', function () {

        return {
            templateUrl: "Templates/MovieSchedule/screenschededit.html"
        }
    });

})();