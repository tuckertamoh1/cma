﻿(function () {
    'use strict';

    angular.module('app.routes', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider
            .when('/', {
                //templateUrl: 'Templates/index.html'
                templateUrl: 'Templates/Login/login.html'
            })
            .when('/index', {
                templateUrl: 'Templates/index.html'
            })
		    .otherwise({
		        redirectTo: '/'
		    });
    }]);


})();
